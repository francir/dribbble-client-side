Dribbble Client Side 
===

### Objetivo
Criar um sistema web de consulta na API do Dribbble 

### O Desafio 
Implementar uma aplicação completamente client-side, que consulte a API do Dribbble e mostre os shots mais populares. Esta aplicação deve funcionar nos navegadores mais recentes do mercado.  
[API Dribbble](http://developer.dribbble.com/v1/)

###### Instalar as dependências
```sh
npm install bower gulp -g
npm install && bower install
```

###### Executar o projeto
```sh
gulp server
```